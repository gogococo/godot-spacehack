extends Button

func _ready():
	pass

func _on_Button_pressed():
	print("Multiplayer? ", $"../MultiplayerCheckbox".is_pressed())
	print("Wants to host? ", $"../MultiplayerOptions/HostCheckbox".is_pressed())
	print("Join game code { ", $"../MultiplayerOptions/JoinGame/HostAddress".text, " }")
	print("Host game code { ", $"../MultiplayerOptions/HostGame/HostAddress".text, " }")
	get_tree().change_scene("res://scenes/Game.tscn")

func generate_random_host_code():
	randomize()
	return(randi() % 50000 + 1)

func _on_HostCheckbox_toggled(button_pressed):
	if button_pressed:
		$"../MultiplayerOptions/JoinGame".visible = false
		$"../MultiplayerOptions/HostGame".visible = true
		$"../MultiplayerOptions/HostGame/HostAddress".text = str(generate_random_host_code())
	else:
		$"../MultiplayerOptions/JoinGame".visible = true
		$"../MultiplayerOptions/HostGame".visible = false

func _on_MultiplayerCheckbox_toggled(button_pressed):
	if button_pressed:
		$"../MultiplayerOptions".visible = true
	else:
		$"../MultiplayerOptions".visible = false
