# godot-spacehack


## Set Up
* Install GotDot Engine
  * https://godotengine.org/download/osx
  * If osx refuses to let you open it, work around is to navigate to Downloads folder were it is, right click & open ``¯\_(ツ)_/¯``

## Run Game
* Open GoDot Engine
  * Navigate to Scene
  * Find Game (root node) & click the scroll button to get to game script
  * Hit play in the top right corner  

## Export
* Go to https://godotengine.org/download/
* Download `Export Templates (Standard)`
* `mv Godot_v3.2.1-stable_export_templates.tpz Godot_v3.2.1-stable_export_templates.zip`
* `mv Godot_v3.2.1-stable_export_templates.zip /Users/{User}/Library/Application Support/Godot/templates/`
* `unzip Godot_v3.2.1-stable_export_templates.zip`
* `mv ./templates 3.2.1.stable`

Path should look like: `/Users/{User}/Library/Application Support/Godot/templates/3.2.1.stable`
In GoDot
* Project -> Export
* Verify Path is correct
* Add OSX, Linux, HTML5



## Deploy
* To Do :(

## Infrastructure
* To Do :(
