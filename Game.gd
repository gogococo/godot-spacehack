extends Node2D

const TILE_SIZE = 48

const LEVEL_SIZES = [
	Vector2(30, 30),
	Vector2(35, 35),
	Vector2(40, 40),
	Vector2(45, 45),
	Vector2(50, 50),
]

const LEVEL_ROOM_COUNTS = [5, 7, 9, 12, 15]
const LEVEL_ITEM_COUNTS = 8
const MIN_ROOM_DIMENSION = 5
const MAX_ROOM_DIMENSION = 8
const TIME_LEFT_SECONDS = 300
const BAD_ITEM_POINTS = 25
const KEY_ITEM_POINTS = 25

#enum Tile {Stars, Door, HallTile, Wall, Ground, Wall2, Portal}
enum Tile {Stars, Door, HallTile, Wall, Ladder, Ground, Wall2, Portal}

var items_array = []

var key_items = {}
var bad_items_picked_up = 0

var has_lightbulb = false

var is_paused = false

# Trying to add random items


class Item extends Reference:
	var sprite_node
	var tile

	func _init(game, x, y, texture):
		#self.is_robot = is_robot
		tile = Vector2(x, y)
		sprite_node = Sprite.new()
		sprite_node.texture = texture
		# Making the sprite filename the node name, don't like
		# it but it works assuming:
		# source_dir = res://items/ and
		# file extension = 3 characters long
		var path = str(texture.resource_path)
		path.erase(path.length() - 3, 3)
		path.erase(0, 11)
		sprite_node.name = path

		sprite_node.position = tile * TILE_SIZE
		sprite_node.offset = Vector2(TILE_SIZE/2, TILE_SIZE/2)
		game.add_child(sprite_node)

	func remove():
		sprite_node.queue_free()



# Current Level ---------------------

var level_num = 0
var map = []
var rooms = []
var level_size
var items = []

# Node Refs -------------------------

onready var tile_map = $TileMap
onready var visibility_map = $VisibilityMap
onready var player = $Player

# Game State -------------------------

var player_tile
var score = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	randomize()
	build_level()
	$CanvasLayer/KeyItems.visible = true
	$CanvasLayer/LevelLocked.text = ""
	$CanvasLayer/KeyItems.text = ""

func _input(event):
	# Only allow movement if neither win nor lose
	# screen is visible
	if not $CanvasLayer/Win.visible and not $CanvasLayer/Lose.visible:
		if !event.is_pressed():
			return
		elif event.is_action_pressed("pause"):
			toggle_pause()
		elif event.is_action("ui_left"):
			try_move(-1, 0)
		elif event.is_action("ui_right"):
			try_move(1, 0)
		elif event.is_action("ui_up"):
			try_move(0, -1)
		elif event.is_action("ui_down"):
			try_move(0, 1)

func try_move(dx, dy):
	
	$CanvasLayer/LevelLocked.visible = false
	var x = player_tile.x + dx
	var y = player_tile.y + dy

	var tile_type = Tile.Stars
	if not is_paused:
		if x >=0 && x < level_size.x && y >= 0 && y < level_size.y:
			tile_type = map[x][y]

	match tile_type:
		Tile.Ground:
				player_tile = Vector2(x, y)
				pickup_items()

		Tile.HallTile:
				player_tile = Vector2(x, y)

		Tile.Door:
			set_tile(x, y, Tile.Ground)
			yield(get_tree().create_timer(.1), "timeout")

		Tile.Portal:
			if has_found_key_items():
				level_num += 1
				score += int(round(timer.get_time_left()))
				if level_num < LEVEL_SIZES.size():
					build_level()
					yield(get_tree().create_timer(.1), "timeout")
					call_deferred("update_visuals")
				else:
					game_won()
			else:
				# TODO Fix layer used
				$CanvasLayer/LevelLocked.visible = true
				$CanvasLayer/LevelLocked.text = ("              You need to find the two key items to proceed.")

	call_deferred("update_visuals")

func pickup_items():
	var remove_queue = []
	for item in items:
		if item.tile == player_tile:
			item.remove()
			remove_queue.append(item)
	for item in remove_queue:
		# Example check if item (cube) has been picked up
		#print("Has cube been picked up? ", has_item_been_picked_up("cube", item))
		if has_item_been_picked_up("lightbulb", item):
			add_lightbulb_to_hud_bar(item)
			has_lightbulb = true
			clear_vision()
		if item.sprite_node.name in key_items.keys():
			key_items[item.sprite_node.name] = 1
			score += KEY_ITEM_POINTS
			for sprite in $CanvasLayer/KeyItems.get_children():
				if item.sprite_node.name in sprite.texture.resource_path:
					sprite.modulate = Color(1,1,1,1)
		else:
			if not has_item_been_picked_up("lightbulb", item):
				score -= BAD_ITEM_POINTS
				bad_items_picked_up += 1
		items.erase(item)

func impair_vision():
	$Player/Impairment.visible = true
	$CanvasLayer/LightsOut.visible = true

func clear_vision():
	$Player/Impairment.visible = false
	$CanvasLayer/LightsOut.visible = false
	#has_lightbulb = true

func build_level():
	# Start with a blank map
	
	rooms.clear()
	map.clear()
	tile_map.clear()
	key_items = {}
	bad_items_picked_up = 0
	$CanvasLayer/KeyItems.visible = true
	clear_hud_items_except_lightbulb()
	if not has_lightbulb:
		impair_vision()

	for item in items:
		item.remove()
	# Moving this outside the loop seems to have fixed items
	# hanging around after the level changes
	# Calling items.clear() in the first iteration may have
	# caused the item.remove() to become useless and left
	# item sprites on the map; even though the sprites are
	# there, they can't be interacted with
	items.clear()

	level_size = LEVEL_SIZES[level_num]
	for x in range(level_size.x):
		map.append([])
		for y in range(level_size.y):
			map[x].append(Tile.Stars)
			tile_map.set_cell(x, y, Tile.Stars)
			visibility_map.set_cell(x, y, 0)

	var free_regions = [Rect2(Vector2(2 ,2), level_size - Vector2(4, 4))]
	var num_rooms = LEVEL_ROOM_COUNTS[level_num]
	for i in range(num_rooms):
		add_room(free_regions)
		if free_regions.empty():
			break
	connect_rooms()

	# Place Player
	var start_room = rooms.front()
	var player_x = start_room.position.x + 1 + randi() % int(start_room.size.x - 2)
	var player_y = start_room.position.y + 1 + randi() % int (start_room.size.y - 2)
	player_tile = Vector2(player_x, player_y)
	yield(get_tree().create_timer(.1), "timeout")
	call_deferred("update_visuals")
	
	# Place end portal
	var end_room = rooms.back()
	var portal_x = end_room.position.x + 1 + randi() % int(end_room.size.x - 2)
	var portal_y = end_room.position.y + 1 + randi() % int(end_room.size.y - 2)
	set_tile(portal_x, portal_y, Tile.Portal)
	
	# Get all ground tiles to provide list of available cells to
	# place items on
	var ground_tiles = tile_map.get_used_cells_by_id(Tile.Ground)
	# Remove player and portal tiles as available cells
	ground_tiles.remove(tile_map.get_cell(player_tile.x, player_tile.y))
	ground_tiles.remove(tile_map.get_cell(portal_x, portal_y))
	ground_tiles.shuffle()
	
	# Place Items
	var num_items = LEVEL_ITEM_COUNTS
	load_items_into_array()
	items_array.shuffle()

	# Make sure lightbulb is spawned on the first level
	if level_num == 0:
		for i in len(items_array):
			if "lightbulb" in items_array[i].resource_path:
				var lightbulb = items_array[i]
				items_array.remove(i)
				items_array.push_front(lightbulb)
				break

	for i in range(num_items):
		# Limits item generation to only one of every item in the
		# res://items directory
		if items_array.size() > 0:
			var x = ground_tiles[0].x
			var y = ground_tiles[0].y
			# Stop lightbulb from spawning if it's already been picked
			# up previously
			if "lightbulb" in items_array[0].resource_path and has_lightbulb\
				and level_num > 0:
				items_array.remove(0)
				print("DEBUG: Removed extra lightbulb spawn")
			items.append(Item.new(self, x, y, items_array[0]))
			# Remove cell so items don't spawn on each other
			ground_tiles.remove(0)
			# Remove item to allow only one instance of each item
			# per level
			items_array.remove(0)
		# Finally found helper methods to extract the filename from a path...
		var item_filename = items_array[0].resource_path.get_file().get_basename()
		if key_items.size() < 2 and not "lightbulb" in item_filename:
			key_items[item_filename] = 0
			add_item_to_hud_bar(items_array[0].resource_path)
	print("DEBUG: Number of items spawned: ", str(items.size()))
	print("DEBUG: Key items: ", key_items)

	$CanvasLayer/Level.text = "Level: " + str(level_num)
	# Start countdown
	set_lose_timer()

var timer = Timer.new() 
func _process(delta):
	if bad_items_picked_up >= 5 or score < 0:
		game_lost()
	$CanvasLayer/Time.text = "Time: " + (str(int(timer.get_time_left())))
	#$CanvasLayer/KeyItems.text = "Key items: " + str(key_items)
	
func set_lose_timer():
	#timer = Timer.new()
	timer.set_one_shot(true)
	timer.connect("timeout",self,"_on_lose_timer_timeout") 
	timer.wait_time = TIME_LEFT_SECONDS # in seconds
	add_child(timer)
	timer.start()

func _on_lose_timer_timeout():
	#var time_left = timer.get_time_left()
	#$CanvasLayer/Lose.visible = true
	game_lost()
	#$CanvasLayer/Level.text = "Time: " + str(Timer.get_time_left())

func update_visuals():
	player.position = player_tile * TILE_SIZE
	var player_center = tile_to_pixel_center(player_tile.x, player_tile.y)
	var space_state = get_world_2d().direct_space_state
	for x in range(level_size.x):
		for y in range(level_size.y):
			if visibility_map.get_cell(x,y) ==0:
				var x_dir = 1 if x < player_tile.x else -1
				var y_dir = 1 if y < player_tile.y else -1
				var test_point = tile_to_pixel_center(x, y) + Vector2(x_dir, y_dir) * TILE_SIZE / 2

				var occlusion = space_state.intersect_ray(player_center, test_point)
				if !occlusion || (occlusion.position - test_point).length() < 1:
					visibility_map.set_cell(x, y, -1)

	for item in items:
		item.sprite_node.position = item.tile * TILE_SIZE
		if !item.sprite_node.visible:
			var item_center = tile_to_pixel_center(item.tile.x, item.tile.y)
			var occlusion = space_state.intersect_ray(player_center, item_center)
			if !occlusion:
				item.sprite_node.visible = true

	#$CanvasLayer/HP.text = "HP: " + str(player_hp)
	$CanvasLayer/Score.text = "Score: " + str(score)

func tile_to_pixel_center(x, y):
	return Vector2((x + 0.5) * TILE_SIZE, (y + 0.5) * TILE_SIZE)

func connect_rooms():
	# Build an AStar graph of the area where we want to add corridors

	var star_graph = AStar.new()
	var point_id = 0
	for x in range(level_size.x):
		for y in range(level_size.y):
			if map[x][y] == Tile.Stars:
				star_graph.add_point(point_id, Vector3(x, y, 0))

			# Connect if Also Star
			if x > 0 && map[x - 1][y] == Tile.Stars:
				var left_point = star_graph.get_closest_point(Vector3(x - 1, y, 0))
				star_graph.connect_points(point_id, left_point)

			# Connect to above if also star
			if y > 0 && map[x][y - 1] == Tile.Stars:
				var above_point = star_graph.get_closest_point(Vector3(x, y - 1, 0))
				star_graph.connect_points(point_id, above_point)

			point_id += 1

	var room_graph = AStar.new()
	point_id = 0
	for room in rooms:
		var room_center = room.position + room.size / 2
		room_graph.add_point(point_id, Vector3(room_center.x, room_center.y, 0))
		point_id += 1

		while !is_everything_connected(room_graph):
			add_random_connection(star_graph, room_graph)

func is_everything_connected(graph):
	var points = graph.get_points()
	var start = points.pop_back()
	for point in points:
		var path = graph.get_point_path(start, point)
		if !path:
			return false

	return true

func add_random_connection(star_graph, room_graph):

	# pick rooms to connect

	var start_room_id = get_least_connected_point(room_graph)
	var end_room_id = get_nearest_unconnected_point(room_graph, start_room_id)

	# Pick Door Locations

	var start_position = pick_random_door_location(rooms[start_room_id])
	var end_position = pick_random_door_location(rooms[end_room_id])

	# Find a path to connect the doors to each other

	var closest_start_point = star_graph.get_closest_point(start_position)
	var closest_end_point = star_graph.get_closest_point(end_position)

	var path = star_graph.get_point_path(closest_start_point, closest_end_point)
	assert(path)

	set_tile(start_position.x, start_position.y, Tile.Door)
	set_tile(end_position.x, end_position.y, Tile.Door)

	for position in path:
		set_tile(position.x, position.y, Tile.HallTile)

	room_graph.connect_points(start_room_id, end_room_id)

func get_least_connected_point(graph):
	var point_ids = graph.get_points()

	var least
	var tied_for_least = []

	for point in point_ids:
		var count = graph.get_point_connections(point).size()
		if !least || count < least:
			least = count
			tied_for_least = [point]
		elif count == least:
			tied_for_least.append(point)

	return tied_for_least[randi() % tied_for_least.size()]

func get_nearest_unconnected_point(graph, target_point):
	var target_position = graph.get_point_position(target_point)
	var point_ids = graph.get_points()

	var nearest
	var tied_for_nearest = []

	for point in point_ids:
		if point == target_point:
			continue

		var path = graph.get_point_path(point, target_point)
		if path:
			continue

		var dist = (graph.get_point_position(point) - target_position).length()
		if !nearest || dist < nearest:
			nearest = dist
			tied_for_nearest = [point]
		elif dist == nearest:
			tied_for_nearest.append(point)

	return tied_for_nearest[randi() % tied_for_nearest.size()]

func pick_random_door_location(room):
	var options = []

	# Top & Bottom Walls

	for x in range(room.position.x + 1, room.end.x - 2):
		options.append(Vector3(x, room.position.y, 0))
		options.append(Vector3(x, room.end.y - 1, 0))

	# Left & Right Walls

	for y in range(room.position.y + 1, room.end.y - 2):
		options.append(Vector3(room.position.x, y, 0))
		options.append(Vector3(room.end.x - 1, y, 0))

	return options[randi() % options.size()]

func add_room(free_regions):
	var region = free_regions[randi() % free_regions.size()]

	var size_x = MIN_ROOM_DIMENSION
	if region.size.x > MIN_ROOM_DIMENSION:
		size_x += randi() % int(region.size.x - MIN_ROOM_DIMENSION)

	var size_y = MIN_ROOM_DIMENSION
	if region.size.y > MIN_ROOM_DIMENSION:
		size_y += randi() % int(region.size.y - MIN_ROOM_DIMENSION)

	size_x = min(size_x, MAX_ROOM_DIMENSION)
	size_y = min(size_y, MAX_ROOM_DIMENSION)

	var start_x = region.position.x
	if region.size.x > size_x:
		start_x += randi() % int(region.size.x - size_x)

	var start_y = region.position.y
	if region.size.y > size_y:
		start_y += randi() % int(region.size.y - size_y)

	var room = Rect2(start_x, start_y, size_x, size_y)
	rooms.append(room)

	# Top & Bottom Walls
	for x in range(start_x, start_x + size_x):
		set_tile(x, start_y, Tile.Wall)
		set_tile(x, start_y + size_y - 1, Tile.Wall)

	# Side walls, minus very first & very last
	for y in range(start_y + 1, start_y + size_y - 1):
		set_tile(start_x, y, Tile.Wall2)
		set_tile(start_x + size_x - 1, y, Tile.Wall2)

		# flooring inside
		for x in range(start_x + 1, start_x + size_x - 1):
			set_tile(x, y, Tile.Ground)

	cut_regions(free_regions, room)

func cut_regions(free_regions, region_to_remove):
	var removal_queue = []
	var addition_queue = []

	for region in free_regions:
		if region.intersects(region_to_remove):
			removal_queue.append(region)

			var leftover_left = region_to_remove.position.x - region.position.x - 1
			var leftover_right = region.end.x - region_to_remove.end.x - 1
			var leftover_above = region_to_remove.position.y - region.position.y - 1
			var leftover_below = region.end.y - region_to_remove.end.y - 1

			if leftover_left >= MIN_ROOM_DIMENSION:
				addition_queue.append(Rect2(region.position, Vector2(leftover_left, region.size.y)))
			if leftover_right >= MIN_ROOM_DIMENSION:
				addition_queue.append(Rect2(Vector2(region_to_remove.end.x + 1, region.position.y), Vector2(leftover_right, region.size.y)))
			if leftover_above >= MIN_ROOM_DIMENSION:
				addition_queue.append(Rect2(region.position, Vector2(region.size.x, leftover_above)))
			if leftover_below >= MIN_ROOM_DIMENSION:
				addition_queue.append(Rect2(Vector2(region.position.x, region_to_remove.end.y + 1), Vector2(region.size.x, leftover_below)))

	for region in removal_queue:
		free_regions.erase(region)

	for region in addition_queue:
		free_regions.append(region)

func set_tile(x, y, type):
	map[x][y] = type
	tile_map.set_cell(x, y, type)

	#if type == Tile.Ground:
	#	clear_path(Vector2(x, y))

func _on_Button_pressed():
	reset_game()

func load_items_into_array():
	items_array = []
	# Loads all item sprites attached as children to the
	# /Game/Items node
	var items_root_node = $"./Items"
	for child_item in items_root_node.get_children():
		print(child_item.texture.resource_path)
		items_array.append(child_item.texture)

func has_item_been_picked_up(item_name_to_check, sprite):
	return item_name_to_check.to_lower() in sprite.sprite_node.name.to_lower()

func has_found_key_items():
	for i in key_items:
		if key_items[i] != 1:
			return false
	return true

func game_lost():
	$CanvasLayer/Lose.visible = true
	clear_vision()
	timer.stop()
	
func game_won():
	score += 200
	var win_label = $CanvasLayer/Win/Label
	win_label.set_text(win_label.get_text() + "\n\nScore: " + str(score) + "!")
	if not has_lightbulb:
		win_label.set_text(win_label.get_text() + "\n\n***\nYou made it to space without a light.\nYOU are the light.\n***\n")
	clear_vision()
	$CanvasLayer/Win.visible = true
	$CanvasLayer/KeyItems.visible = false
	timer.stop()

func reset_game():
	level_num = 0
	score = 0
	build_level()
	$CanvasLayer/Win.visible = false
	$CanvasLayer/Lose.visible = false
	impair_vision()
	clear_hud_items()

func add_item_to_hud_bar(item):
	var x_pos = 250
	var x_increment = 50
	var key_items_in_hud = get_tree().get_nodes_in_group("key_items_hud")
	if key_items_in_hud.size() > 0:
		x_pos = key_items_in_hud[key_items_in_hud.size() - 1].position.x + x_increment
	var sprite = Sprite.new()
	sprite.texture = load(item)
	sprite.position.x = x_pos
	sprite.position.y = -648
	sprite.visible = true
	sprite.modulate = Color(1,1,1,0.3)
	# Set z-index to be > 1024 as the Player/Impairment light2D affects
	# anything with a z-index up to that
	sprite.z_index = 1025
	sprite.add_to_group("key_items_hud")
	$CanvasLayer/KeyItems.add_child(sprite)

func add_lightbulb_to_hud_bar(item):
	var sprite = Sprite.new()
	sprite.texture = item.sprite_node.texture
	sprite.position.x = 200
	sprite.position.y = -648
	sprite.visible = true
	$CanvasLayer/KeyItems.add_child(sprite)

func clear_hud_items():
	for child in $CanvasLayer/KeyItems.get_children():
		print(child.texture.resource_path)
		$CanvasLayer/KeyItems.remove_child(child)

func clear_hud_items_except_lightbulb():
	for child in $CanvasLayer/KeyItems.get_children():
		if not "lightbulb" in child.texture.resource_path:
			$CanvasLayer/KeyItems.remove_child(child)

func toggle_pause():
	if is_paused:
		timer.paused = false
		is_paused = false
		$CanvasLayer/Pause.visible = false
		if not has_lightbulb:
			impair_vision()
		else:
			clear_vision()
	else:
		timer.paused = true
		is_paused = true
		$CanvasLayer/Pause.visible = true
		clear_vision()
