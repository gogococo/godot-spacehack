# godot-spacehack

## Tutorials
* ✓ Tile Maps ✓
  * https://www.gamefromscratch.com/page/Godot-3-Tutorial-Tiles-and-TileMaps.aspx
* ✓ Building a Roguelike from Scratch in godot ✓
  * https://www.youtube.com/watch?v=vQ1UGbUlzH4
* ✓ Character Animation ✓
  * https://docs.godotengine.org/uk/latest/tutorials/2d/2d_sprite_animation.html
* Project Organization
  * https://docs.godotengine.org/en/3.1/getting_started/workflow/project_setup/project_organization.html
* Networking
  * https://gitlab.com/menip/godot-multiplayer-tutorials/-/blob/master/NetworkingBasics.md
* Multiplayer
  * https://gitlab.com/menip/godot-multiplayer-tutorials/
* Multiplayer Lobby
  * https://www.youtube.com/watch?v=Xu3LtVihYoo
* Cloud deploy via GCP
  * https://gitlab.com/menip/godot-multiplayer-tutorials/-/blob/master/GCPTutorial/GCPTut.md

## Assets
* Elthen's female adventurer
  * https://www.patreon.com/elthen/posts?filters[tag]=BiMed%20Characters
* Space tiles
  * https://opengameart.org/sites/default/files/scifi_space_rpg_tiles.png
* Alternate space tiles
  * https://v-ktor.itch.io/32x32-rpg-tilesets
* Humble Bundle tile set
  * https://www.humblebundle.com/software/7000-game-dev-icons-software?hmb_source=humble_home&hmb_medium=product_tile&hmb_campaign=mosaic_section_2_layout_index_4_layout_type_twos_tile_index_2_c_7000gamedevicons_softwarebundle
